public class Square {
    private Integer side;

    public Square(Integer side) {
        this.side = side;
    }
    public Square() {
    }

    public Integer getSide() {
        return side;
    }

    public void setSide(Integer side)  {
        this.side = side;
    }

    public int getPerimeter (Integer side) throws Exception {
        if (side <= 0) throw new Exception("Не правильное число");
        return side * 4;
    }
}
