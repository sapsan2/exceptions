import java.util.Scanner;

public class Array {
    private Integer [] arr;


    public Array(Integer[] arr) {
        this.arr = arr;
    }

    public Array() {
    }

    public Integer[] getArr() {
        return arr;
    }

    public void setArr(Integer[] arr) {
        this.arr = arr;
    }

    public Integer[] fillArray (Scanner scanner) {
        System.out.println("Введите значения массива!");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        return arr;
    }

    public void showArr(Integer index){
        try {
            System.out.println(arr[index - 1]);
        } catch (IndexOutOfBoundsException e) {
            System.err.println("За пределами массива!");
        }
    }
}
