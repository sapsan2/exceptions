import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите сторону квадрата");
        Integer side = scanner.nextInt();
        Square square = new Square();

        try {
            System.out.println("Периметр = " + square.getPerimeter(side));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        System.out.println("Введите размер массива");
        int size = scanner.nextInt();
        Integer[] arr = new Integer[size];
        Array array = new Array(arr);
        array.fillArray(scanner);
        System.out.println("Введите индекс числа массива");
        int index = scanner.nextInt();
        array.showArr(index);

        Numbers numbers = new Numbers();
        System.out.println("Введите номер?");
        numbers.getPhoneNumber(scanner);
    }
}